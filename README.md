# How to use

* Correct gfx/ link to raw/ folder
* Make a hard link for server JSON files (`ln /path/to/server/*.json .`)
* Launch in Ren'Py 7.
