﻿# The script of the game goes in this file.
define e = Character("SDK")

label start:
    scene black
    centered "Click to load game data"
    $ GAME_LOADER()

label loop:
    menu:
        "Quest editor":
            jump quest_editors
        "Units editor":
            jump units_editors
        "Quit":
            return

label restore:
    menu:
        "Save changes":
            python:
              try:
                f=open(get_path("quests.editor.json"), "r")
                alltquests["Main"]=json.load(f)
                f.close()
                f=open(get_path("quests.json"), "w")
                json.dump(alltquests, f, indent=1, separators=(',', ': '))
                f.close()
                print("Quests saved!")
              except:
                print("No quests to save")
                pass
              try:
                f=open(get_path("units.editor.json"), "r")
                allunitsbase=json.load(f)
                f.close()
                f=open(get_path("units.json"), "w")
                json.dump(allunitsbase, f, indent=1, separators=(',', ': '))
                f.close()
                print("Units saved!")
              except:
                print("No units to save")
                pass
            pass
        "Discard changes":
            $ GAME_LOADER()
            pass
        "Continue editing":
            pass
    jump loop

